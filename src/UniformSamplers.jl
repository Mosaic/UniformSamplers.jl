# Copyright (C) 2024 Dominik Itner
# UniformSamplers.jl is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
# UniformSamplers.jl is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.
# You should have received a copy of the GNU Lesser General Public License along with UniformSamplers.jl.  If not, see <http://www.gnu Lesser.org/licenses/>.
# A copy of the GNU Lesser General Public License has deliberately not been included to circumvent unnecessary traffic due to the high degree of modularity and interoperability of UniformSamplerss in Mosaic.jl.
# The license can instead be found at <https://codeberg.org/Mosaic/License>.

module UniformSamplers

# External dependencies

using SamplerBase.Core: Sampler
using SamplerBase.Interface: samples

import SamplerBase.Interface as SamplerInterface

# Super type

abstract type AbstractUniformSampler <: Sampler end

# Implementation

struct UniformSampler{T} <: AbstractUniformSampler
	samples::T
end

"""
	UniformSampler(nsamples, ndims)

Generate a random `UniformSampler` with `nsamples` samples in `ndims` dimensions with a uniform random distribution `x ∈ [0, 1]`.
"""
UniformSampler(nsamples, ndims) =
	UniformSampler(rand(ndims, nsamples))

"""
	uniform(nsamples, ndims)

"""
uniform(nsamples, ndims) =
	UniformSampler(nsamples, ndims)

# Extend Interface

## Sampler: length

# Length is simply the size of the underlying matrix
SamplerInterface.length(sampler::UniformSampler) =
	size(sampler.samples, 2)

## Sampler: ndims

# Number of dimensions is simply the size of the underlying matrix
SamplerInterface.ndims(sampler::UniformSampler) =
	size(sampler.samples, 1)

## Sampler: getindex

# Access samples in `UniformSampler` via linear indices
SamplerInterface.getindex(sampler::UniformSampler, i) =
	sampler.samples[:,i]

## Sampler: samples

# Fetch all samples
SamplerInterface.samples(sampler::UniformSampler) =
	sampler.samples

end # UniformSamplers
