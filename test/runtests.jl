using Test
using UniformSamplers: uniform, samples

# Initialize uniform sampler
sampler = uniform(100, 3)

# Test number of samples
@test length(sampler) == 100

# Test number of dimensions
@test ndims(sampler) == 3

# Test getindex
@test sampler[10] == samples(sampler)[:,10]

# Test iteration
let i = 0
	for _ in sampler
		i = i + 1
	end
	@test i == length(sampler)
end